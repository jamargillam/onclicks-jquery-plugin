/*
	@Author: Jamar Gillam
	@Name: Onclicks plugin
	@Version: 1.0 (Stable)
	@Description: This simple plugin simply passes in parameters into the 'clt' function for page tracking purposes. 
				  An onclick attribute is attached to a specified DOM element. Works on Cvent properties only.
	@Dependencies: jQuery library is required. You can grab a copy of jQuery from the Google API website: https://developers.google.com/speed/libraries/#jquery.

*/
(function ($) {
 
    $.fn.onclicks = function( options ) {
 
        //Default settings for the plugin
        var settings = $.extend({
            parameter1: '', //First parameter of the clt function
            parameter2: '', //Second parameter for the clt function
            useInnerHtmlOnly: false, //Use only the inner text or html of an anchor tag
            useInnerHtmlParam1: false, //Use the innerhtml and one parameter
            useParametersOnly: false //Use parameters only
        }, options );
 
        return this.each( function() {
	        try{	        	
		        if( settings.useInnerHtmlOnly ){
		        	/*****************************************************
	        		*
	        		*			@InnerHTML use only { clt(innerHTML) }
	        		*
	        		*****************************************************/
			        var thisInnerHtml = this.innerHTML;
					var clt = "clt('"+ thisInnerHtml +"')";
					//Set the value of the 'onclick' attribute
					this.setAttribute('onclick', clt);
									
		        } else if( settings.useInnerHtmlParam1 ){
		        	/**********************************************************************************
		        	*
		        	*			@InnerHTML and parameter1 use only { clt(innerHTML, 'parameter1') }
		        	*
		        	***********************************************************************************/				
					if( typeof settings.parameter1 != "undefined" && settings.parameter1 !== ""){
						var thisInnerHtml = this.innerHTML;
						var clt = "clt('"+ thisInnerHtml + "','" + settings.parameter1 +"')";
						//Set the value of the 'onclick' attribute
						this.setAttribute('onclick', clt);
					}else{
						console.error("Variable 'parameter1' is undefined."); 
					}		
			        
		        }else if( settings.useParametersOnly ){
		        	/************************************************************************
		        	*
		        	*			@Parameters use only { clt('parameter1', 'parameter2') }
		        	*
		        	************************************************************************/
			        if( settings.parameter1 !== "" && settings.parameter2 !== ""){
				       /***** clt('paremeter1', 'parameter2') *****/
						var clt = "clt('"+ settings.parameter1 + "','" + settings.parameter2 +"')";
						//Set the value of the 'onclick' attribute
						this.setAttribute('onclick', clt);
						
				     } else if( settings.parameter1 !== "" && settings.parameter2 === ""){
				     	/***** clt('paremeter1') *****/
						var clt = "clt('"+ settings.parameter1 + "')";
						//Set the value of the 'onclick' attribute
						this.setAttribute('onclick', clt);
						
				     } else if( settings.parameter1 === "" && settings.parameter2 !== ""){
				     	/***** clt('parameter2') *****/
						var clt = "clt('"+ settings.parameter2 + "')";
						//Set the value of the 'onclick' attribute
						this.setAttribute('onclick', clt);
						
				     }else{
					    console.error("Please pass in at least one parameter into the plugin. Use the 'parameter1' or 'parameter2' to pass in the arguments."); 					     
				     }
				      		       
		        } else{
		        		/********************************************
		        		*
		        		*			@Default parameters
		        		*
		        		*********************************************/
			        	var clt = "clt('Argument1','Argument2')";
			        	//Set the value of the 'onclick' attribute
						this.setAttribute('onclick', clt);			        
		        }
	        }catch(error){
		        console.error("Error: " + error.message);
	        }
        });// #End the return each function
 
    };// #End the onclicks function
 
}( jQuery ));